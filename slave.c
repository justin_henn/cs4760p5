//slave
//This is the slave file for the oss/slave program
//Justin Henn
//Assignment 5
//4/10/17
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <sys/types.h>
#include <signal.h>
#include <sys/shm.h>
#include <sys/stat.h>
#include <errno.h>
#include <sys/msg.h>
#include <unistd.h>
#include "msgqueue.h"
#define PERM (S_IRUSR | S_IWUSR)
#define NUM_RESOURCES 20
#define BOUND 50001
#define MULTIPLY_FOR_SECOND 1000000000
#define MULTIPLY_FOR_MILLI 1000000

//structure for message

typedef struct {
   long mtype;
   char mtext[1024];
} mymsg_t;

typedef struct resource {

   char resource_name[1024];
   long request[18];
   long allocation[18];
   long release[18];
   long total_resources;
   long all_available;
}resource_t;

//timer for when to release or request resource

int when_to_resource(int* s, int* n) {

  return (rand() % BOUND) + ((*s * MULTIPLY_FOR_SECOND) + *n);

}

//choose what resource to request

int choose_resource_to_request(resource_t* res, int ele) {

  while(1) {

    int i = rand() % NUM_RESOURCES;
    if ((&res[i])->request[ele] < (&res[i])->all_available)
      return i;
  }
}

//choose what resource to release

int choose_resource_to_release(resource_t* res, int ele) {

  int choose_arr[NUM_RESOURCES];
  int i = rand() % NUM_RESOURCES;


  while ((&res[i])->allocation[ele] == 0 || (&res[i])->release[ele] == (&res[i])->allocation[ele] ) {

      i = rand() % NUM_RESOURCES;
  }
  (&res[i])->release[ele]++;
  return i;
}


//choose to release or request

int what_to_do(resource_t* res, int ele) {

  int i;
  for(i = 0; i < NUM_RESOURCES; i++) {

    if ((&res[i])->allocation[ele] > 0 && (&res[i])->release[ele] < (&res[i])->allocation[ele] ) {

      return rand() % 2;
    }
  }
  return 0;
}

//timer for when to check to terminate

int get_term_time(int* s, int* n) {

   return ((rand() % 251) * MULTIPLY_FOR_MILLI) + ((*s * MULTIPLY_FOR_SECOND) + *n);
}

//odds of terminating when checking timer

int should_proc_term() {

  return rand() % 2;

}



int main (int argc, char **argv) {

  key_t key_seconds, key_pcb_info, key_nano_seconds, child_queue_key, parent_queue_key, kill_queue_key, key_resources, key_pids, time_queue_key, deadlock_queue_key, deadlockrec_queue_key;
  key_pcb_info = 3423563;
  key_nano_seconds = 12345;
  child_queue_key = 85733;
  parent_queue_key = 922435;
  key_seconds = 84323;
  kill_queue_key = 000001;
  key_resources = 010001;
  key_pids = 100011;
  time_queue_key = 100012;
  deadlock_queue_key = 100022;
  deadlockrec_queue_key = 100033;
  int* seconds;
  int* nano_seconds;
  int* pids;
  //int proc_num = atoi(argv[1]);
  //int num_incr = atoi(argv[3]);
  //int number_of_proc = atoi(argv[4]);
  int shm_id_pcb_info, i, shm_id_nano_seconds, j, shm_id_seconds, request_or_release = 5, time_to_change_resources, shm_id_resources, shm_id_pids;
  //FILE* logfile;
  //struct timespec tps;
  static int child_queueid, parent_queueid, kill_queueid, time_queueid, deadlock_queueid, deadlockrec_queueid;
  int size;
  mymsg_t mymsg, sentmsg, parentmsg, timemsg;
  srand(time(NULL));
  int full_nano = 0, timer = 0, x = 0, what_time_to_resource = 0, what_resource_to_release, what_resource_to_request, time_to_check_for_term;
  int what_proc_am_i, what_message_to_check, proc_num = atoi(argv[1]);
  //pcb_info_t* pcb_for_project;
  resource_t* all_resources;
  srand(time(NULL));

//open shared memory

  if ((shm_id_pids = shmget(key_pids, proc_num*sizeof(int), PERM)) == -1) {
    perror("shmget");
    return 1;
  }
  if ((pids = (int*)shmat(shm_id_pids, NULL, 0)) == (void*)-1) {
    perror("pids Failed to attach shared memory segment");
    return 1;
  }

  if ((shm_id_resources = shmget(key_resources, NUM_RESOURCES*sizeof(resource_t), PERM)) == -1) {
    perror("shmget");
    return 1;
  }
  if ((all_resources = (resource_t *)shmat(shm_id_resources, NULL, 0)) == (void*)-1) {
    perror("Failed to attach shared memory segment");
    return 1;
  }


  if ((shm_id_seconds = shmget(key_seconds, sizeof(int), PERM)) == -1) {
    perror("shmget");
    return 1;
  }
  if ((seconds = (int *)shmat(shm_id_seconds, NULL, 0)) == (void *)-1) {
    perror("Failed to attach shared memory segment");
    return 1;
  }


  if ((shm_id_nano_seconds = shmget(key_nano_seconds, sizeof(int), PERM)) == -1) {
    perror("shmget");
    return 1;
  }
  if ((nano_seconds = (int *)shmat(shm_id_nano_seconds, NULL, 0)) == (void *)-1) {
      perror("Failed to attach shared memory segment");
      return 1;
  }
  child_queueid = initqueue(child_queue_key, child_queueid);
  parent_queueid = initqueue(parent_queue_key, parent_queueid);
  kill_queueid = initqueue(kill_queue_key, kill_queueid);
  time_queueid = initqueue(time_queue_key, time_queueid);
  deadlock_queueid = initqueue(deadlock_queue_key, deadlock_queueid);
  deadlockrec_queueid = initqueue(deadlockrec_queue_key, deadlockrec_queueid);

//get your element in the pids array

  while (pids[x] != getpid())
   x++;

  int what_is_my_element = x;
  timemsg.mtype = 1;

//message to enter critical section to get inintial timers

  if ((msgrcv(time_queueid, &timemsg, 1024, 0, 0)) >=0) {
    ;
  }

  /*if ((msgread(time_queueid, &timemsg, 1024)) == -1) {
     perror("Failed to read message queue");
     return 1;
  }*/

  //printf("hello");
  what_time_to_resource = when_to_resource(seconds, nano_seconds);
  time_to_check_for_term = get_term_time(seconds, nano_seconds);

  if(msgsnd(time_queueid, &timemsg, 1024,0) == -1) {
    perror("Failed write");
    return 1;
  }
  sentmsg.mtype = getpid();

//main loop

  while (1) {

//message to enter critical section

    if ((msgrcv(time_queueid, &timemsg, 1024, 0, 0)) >=0) {
      ;
    }

    //printf("hello\n");

//timer met for doing a resource request or release

    if (((*seconds * MULTIPLY_FOR_SECOND) + *nano_seconds) >= what_time_to_resource) {


      what_time_to_resource = when_to_resource(seconds, nano_seconds);
      request_or_release = what_to_do(all_resources, what_is_my_element);

//release a resource

      if(request_or_release == 1) {

        what_resource_to_release = choose_resource_to_release(all_resources, what_is_my_element);


  //sentmsg.mtype = getpid();
        strcpy(sentmsg.mtext, (&all_resources[what_resource_to_release])->resource_name);

        if(msgsnd(child_queueid/*release queue*/, &sentmsg, 1024,0) == -1) {
          perror("Failed write");
          return 1;
        }


      }

//request a resource

      else {

        what_resource_to_request = choose_resource_to_request(all_resources, what_is_my_element);

        strcpy(sentmsg.mtext, (&all_resources[what_resource_to_request])->resource_name);


  //sentmsg.mtype = getpid();
        //printf("%d\n", sentmsg.mtype);
        if(msgsnd(parent_queueid/*request queue*/, &sentmsg, 1024,0) == -1) {
          perror("Failed write");
          return 1;
        }
      }
    }

//see if process should die because of deadlock

    if ((msgrcv(deadlock_queueid, &mymsg, 1024, getpid(), IPC_NOWAIT)) >=0) {

      if(msgsnd(deadlockrec_queueid, &sentmsg, 1024,0) == -1) {
        perror("Failed write");
        return 1;
      }
      break;
    }

//check to see if it should terminate successfully

    if (((*seconds * MULTIPLY_FOR_SECOND) + *nano_seconds) >= time_to_check_for_term) {

      if (should_proc_term() == 1) {

        if(msgsnd(kill_queueid/*term queue*/, &sentmsg, 1024,0) == -1) {
          perror("Failed write");
          return 1;
        }
        break;
      }
      else
        time_to_check_for_term = get_term_time(seconds, nano_seconds);
    }

//message to leave critical section

    if(msgsnd(time_queueid, &timemsg, 1024,0) == -1) {
      perror("Failed write");
      return 1;
    }
  }

//message to leave critical section if process terminates

  if(msgsnd(time_queueid, &timemsg, 1024,0) == -1) {
    perror("Failed write");
    return 1;
  }

  return 0;
}
