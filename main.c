//main
//This is the main file and the master file for the oss/slave program
//Justin Henn
//4/10/17
//Assignment 5
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <sys/types.h>
#include <signal.h>
#include <sys/shm.h>
#include <sys/stat.h>
#include <errno.h>
#include <unistd.h>
#include <sys/msg.h>
#include <memory.h>
#include "msgqueue.h"
#define NUM_RESOURCES 20
#define PERM (S_IRUSR | S_IWUSR)
#define MULTIPLY_FOR_MILLI 1000000
#define MULTIPLY_FOR_SECOND 1000000000
#define SPAWN_PROC ((rand() % 500 + 1) * MULTIPLY_FOR_MILLI) + ((*seconds * MULTIPLY_FOR_SECOND) + *nano_seconds);
#define TOTAL_LINES 100000

//resource struct

typedef struct resource {

   char resource_name[1024];
   long request[18];
   long allocation[18];
   long release[18];
   long total_resources;
   long all_available;
}resource_t;

typedef struct {
   long mtype;
   char mtext[1024];
} mymsg_t;


int num_proc_for_alarm, shm_id_pcb_info, shm_id_nano_seconds, shm_id_seconds, shm_id_resources, shm_id_pids;
int deadlocked_proc_terminated = 0, granted = 0, success_term = 0, detection_ran = 0, proc_deadlocked = 0;
static int child_queueid, parent_queueid, kill_queueid, time_queueid, deadlock_queueid, deadlockrec_queueid;
int* seconds;
int* nano_seconds;
int* pids;
resource_t* all_resources;
FILE* logfile;


//kill child processes

void kill_childs () {

  int y, z = num_proc_for_alarm;
  signal(SIGTERM, SIG_IGN);
  kill(0, SIGTERM);
  for(y = 0; y < num_proc_for_alarm; y++){

    if (wait(NULL) != -1) {

      z--;
    }
  }
}


//detach and remove shared memory

int detachandremove (int shmid, void *shmaddr) {
   int error = 0;

   if (shmdt(shmaddr) == -1)
     error = errno;
   if ((shmctl(shmid, IPC_RMID, NULL) == -1) && !error)
     error = errno;
   if (!error)
      return 0;
   return -1;
}

//print statistics at end of run

void statistics() {

  fprintf(logfile, "\n\nRequests Granted: %d\n", granted);
  fprintf(logfile, "Processes terminated by deadlock: %d\n", deadlocked_proc_terminated);
  fprintf(logfile, "Processes not terminated by deadlock %d\n", success_term);
  fprintf(logfile, "How many times deadlock detection was run: %d\n", detection_ran);
  fprintf(logfile, "Average: %.2f\n", (double)deadlocked_proc_terminated / proc_deadlocked);

}

//Alarm handler

void ALARM_handler(int sig) {

/*  int y;
  fprintf(stderr, "Reached alarm\n");
  for(y = 0; y < num_proc_for_alarm; y++)
    kill(point[y], SIGKILL); */


  printf("Alarm Reached\n");
  kill_childs();

  if (detachandremove(shm_id_seconds, seconds) == -1) {
    perror("Failed to destroy shared memory segment");
    exit (1);
  }
  if (detachandremove(shm_id_nano_seconds, nano_seconds) == -1) {
    perror("Failed to destroy shared memory segment");
    exit (1);
  }
  if (detachandremove(shm_id_resources, all_resources) == -1) {
    perror("Failed to destroy shared memory segment");
    exit (1);
  }

  if (detachandremove(shm_id_pids, pids) == -1) {
    perror("Failed to destroy shared memory segment");
    exit (1);
  }

  if(removequeue(parent_queueid) < 0)
    perror("remove queue");
  if(removequeue(child_queueid) < 0)
    perror("remove queue");
  if(removequeue(kill_queueid) < 0)
    perror("remove queue");
  if(removequeue(time_queueid) < 0)
    perror("remove queue");
  if(removequeue(deadlock_queueid) < 0)
    perror("remove queue");
  if(removequeue(deadlockrec_queueid) < 0)
    perror("remove queue");

  statistics();


  fclose(logfile);

  exit(0);
}

//Control C handler

void SIGINT_handler(int sig) {

  signal(sig, SIG_IGN);
  printf("Received Control C\n");
  kill_childs();

  if (detachandremove(shm_id_seconds, seconds) == -1) {
    perror("Failed to destroy shared memory segment");
    exit (1);
  }
  if (detachandremove(shm_id_nano_seconds, nano_seconds) == -1) {
    perror("Failed to destroy shared memory segment");
    exit (1);
  }
  if (detachandremove(shm_id_resources, all_resources) == -1) {
    perror("Failed to destroy shared memory segment");
    exit (1);
  }

  if (detachandremove(shm_id_pids, pids) == -1) {
    perror("Failed to destroy shared memory segment");
    exit (1);
  }

  if(removequeue(parent_queueid) < 0)
    perror("remove queue");
  if(removequeue(child_queueid) < 0)
    perror("remove queue");
  if(removequeue(kill_queueid) < 0)
    perror("remove queue");
  if(removequeue(time_queueid) < 0)
    perror("remove queue");
  if(removequeue(deadlock_queueid) < 0)
    perror("remove queue");

  fclose(logfile);

  exit(0);
}

//fill resources with how many resources of each

void fill_resources (resource_t* res) {

  int percent_shareable_resource = 0, i, y;
  percent_shareable_resource = rand() % 4 + 1;

  for(i = 0; i < NUM_RESOURCES; i++) {

    if(i < percent_shareable_resource) {

      (&res[i])->total_resources = 10000;
      (&res[i])->all_available = (&res[i])->total_resources;
    }

    else {

      (&res[i])->total_resources = rand()% 10 + 1;
      (&res[i])->all_available = (&res[i])->total_resources;
    }
    sprintf((&res[i])->resource_name, "R%d", i);

    for(y = 0; y < 18; y++) {
      (&res[i])->request[y] = 0;
      (&res[i])->allocation[y] = 0;
      (&res[i])->release[y] = 0;
    }
  }
}

//get the pid of the calling message

int get_pid_number (int a[], int proc_num, mymsg_t m) {

  int x;
  for (x = 0; x < proc_num; x++) {

    if(a[x] == m.mtype)
      return x;
  }
  return -1;
}

//get the resource that the calling message wants to release or request

int get_res_num(mymsg_t m, resource_t* res) {

  int y;
  for (y = 0; y < NUM_RESOURCES; y++) {

    if(!strcmp(m.mtext, ((&res[y])->resource_name))) {

      return y;
    }
  }
}

//reclaim resources after a process terminated

void reclaim_resources(resource_t* res, int x) {

  int y;

  for (y = 0; y < NUM_RESOURCES; y++) {

    (&res[y])->total_resources = (&res[y])->total_resources + (&res[y])->allocation[x];
    (&res[y])->allocation[x] = 0;
    (&res[y])->request[x] = 0;
    (&res[y])->release[x] = 0;
  }
}

//giveout resources after a process has been terminated

void giveout_resources(resource_t* res, int total_num_proc, int* num_lines, int verbose) {

  int i, y;
  for (i = 0; i < total_num_proc; i++) {

    for(y = 0; y < NUM_RESOURCES; y++) {

      while ((&res[y])->request[i] > 0) {
        if((&res[y])->total_resources != 0) {

          (&res[y])->total_resources = (&res[y])->total_resources - 1;
          (&all_resources[y])->request[i] = (&all_resources[y])->request[i] - 1;
          granted++;
          if(*num_lines < TOTAL_LINES && verbose == 1) {

            fprintf(logfile, "Master granting %d request %s at time %d:%d\n", pids[i], (&res[y])->resource_name, *seconds, *nano_seconds);
            *num_lines++;
          }
        }
        else
          break;
      }
    }
  }
}

//checks to see if a process can run at some point for deadlock detection

int req_lt_avail(resource_t* res, int current_proc, int avail_vec[], int mark[]) {

  int i, do_not_mark = 0;
  for (i = 0; i < NUM_RESOURCES; i++) {

    if(avail_vec[i] < (&res[i])->request[current_proc])
      do_not_mark = 1;
  }
  if (do_not_mark == 0) {

    for(i = 0; i < NUM_RESOURCES; i++) {

      avail_vec[i] = avail_vec[i] + (&res[i])->allocation[current_proc];
    }
    mark[current_proc] = 1;
    return 0;
  }
  return 1;
}

//main deadlock detection function

int deadlock(resource_t* res, int* proc_num, int pids[], int* num_lines, int total_num_proc, int verbose) {

  int mark[*proc_num], avail_vec[NUM_RESOURCES], i, x, change = 0, res_alloc, detected = 0;
  char proc_str[1024], temp_str[10], res_str[1024];
  mymsg_t deadmsg;

  memset(mark, 0, sizeof(mark));

  for (i = 0; i < NUM_RESOURCES; i++) {

    avail_vec[i] = (&res[i])->total_resources;
  }


  for (i = 0; i < total_num_proc; i++) {

    res_alloc = 0;

    for (x = 0; x < NUM_RESOURCES; x++) {

      if ((&res[x])->allocation[i] != 0)
        res_alloc = 1;
    }
    if (res_alloc == 0) {

     mark[i] = 1;
    }
  }

  while (change == 0) {

    change = 1;

    for(i = 0; i < total_num_proc; i++) {

      if(mark[i] == 1)
        continue;

      else {

        change = req_lt_avail(res, i, avail_vec, mark);
        if(change == 0)
          break;
      }
    }
  }

  sprintf(proc_str, "\tProcesses");
  for(i = 0; i < total_num_proc; i++) {

    if(mark[i] == 0) {

      sprintf(temp_str," %d", pids[i]);
      strcat(proc_str, temp_str);
      detected = 1;
      proc_deadlocked++;
    }
  }
  if (detected == 1) {

    if (*num_lines < TOTAL_LINES) {

      fprintf(logfile, "%s\n", proc_str);
      fprintf(logfile, "\tAttempting to resolve deadlock\n");
      *num_lines = *num_lines + 2;
    }

    for(i = 0; i < total_num_proc; i++) {

      if (mark[i] == 0)
        break;
    }
    if (*num_lines < TOTAL_LINES) {

      fprintf(logfile, "\tKilling process %d\n", pids[i]);
      *num_lines = *num_lines + 1;
    }
    //kill(pids[i], SIGKILL);

    deadmsg.mtype = pids[i];
    if(msgsnd(deadlock_queueid, &deadmsg, 1024,0) == -1) {
      perror("Failed write");
      return 1;
    }
    pids[i] = 0;
    *proc_num = *proc_num - 1;
   // printf("%d\n", *proc_num);
//    int r = 0;
    if ((msgrcv(deadlockrec_queueid, &deadmsg, 1024, 0, 0)) >=0) {
      ;
    }
    deadlocked_proc_terminated++;

    sprintf(res_str,"\t\tResources released are as follows:");
    for (x = 0; x < NUM_RESOURCES; x++) {

      //res_str += sprintf(res_str, " %s:%d", (&res[x])->allocation[i]);
      if((&res[x])->allocation[i] > 0) {
        strcat(res_str, " ");
        strcat(res_str, (&res[x])->resource_name);
        sprintf(temp_str, ":%d", (&res[x])->allocation[i]);
        strcat(res_str, temp_str);
      }
    }
    if (*num_lines < TOTAL_LINES) {

      fprintf(logfile, "%s\n", res_str);
      *num_lines = *num_lines + 1;
    }
    reclaim_resources(res, i);
    giveout_resources(res, total_num_proc, num_lines, verbose);
    return 1;
  }
  return 0;
  //printf("\n");
}

int main (int argc, char **argv) {

  int opt;
  char* filename = NULL;
  int number_proc = 0, verbose = 0, i, x, y, processes_made = 0, z, total_num_of_proc = 8;
  long  unsigned when_to_spawn = 0;
  extern char* optarg;
  //int pids[total_num_of_proc];
  key_t key_pcb_info, key_nano_seconds, key_resources, child_queue_key, parent_queue_key, key_seconds, kill_queue_key, key_pids, time_queue_key, deadlock_queue_key, deadlockrec_queue_key;
  key_pcb_info = 3423563;
  key_seconds = 84323;
  key_nano_seconds = 12345;
  child_queue_key = 85733;
  parent_queue_key = 922435;
  kill_queue_key = 000001;
  key_resources = 010001;
  key_pids = 100011;
  time_queue_key = 100012;
  deadlock_queue_key = 100022;
  deadlockrec_queue_key = 100033;
  //FILE* logfile;
  pid_t child_pid;
  unsigned char bit_vector[3];
  int iter = 0, iter1 = 0, checked_message, overhead = 0, num_lines = 0;
  char send_str[1024];
  srand(time(NULL));

  signal(SIGINT, SIGINT_handler);
  signal(SIGALRM, ALARM_handler);

  mymsg_t mymsg, sentmsg, timemsg;

//Get arguments

  while ((opt = getopt(argc, argv, "hs:l:v:")) != -1) { //this loop looks at the the arguments and processes them appropriately

    switch (opt) {
    case 'h':
      printf("h - help\nl - specify a file name after l for the logfile \ns - number of slave processes\nv - 1 for verbose\n");
      return 0;
    case 's':
      total_num_of_proc = atoi(optarg);
      break;
    case 'l':
      filename = optarg;
      break;
    case 'v':
      verbose  = atoi(optarg);
      break;
    }
  }

//fill argument

  if (filename == NULL) { //if no filename was givien as an argument

    filename = "test.out";
  }

//open file

  if ((logfile = fopen(filename, "w+")) == NULL) { //check to see if it opened a file

    perror("File open error");
    return -1;
  }

//open shared memory

  if ((shm_id_pids = shmget(key_pids, total_num_of_proc*sizeof(int), PERM | IPC_CREAT)) == -1) {
    perror("FAIL pids");
    return 1;
  }
  if ((pids = (int*)shmat(shm_id_pids, NULL, 0)) == (void*)-1) {
    perror("Failed to attach shared memory segment");
    if (shmctl(shm_id_pids, IPC_RMID, NULL) == -1)
      perror("Failed to  remove memory segment");
      return 1;
  }

  if ((shm_id_resources = shmget(key_resources, NUM_RESOURCES*sizeof(resource_t), PERM | IPC_CREAT)) == -1) {
    perror("FAIL");
    return 1;
  }
  if ((all_resources = (resource_t*)shmat(shm_id_resources, NULL, 0)) == (void*)-1) {
    perror("Failed to attach shared memory segment");
    if (shmctl(shm_id_resources, IPC_RMID, NULL) == -1)
      perror("Failed to  remove memory segment");
      return 1;
  }

   if ((shm_id_seconds = shmget(key_seconds, sizeof(int), PERM | IPC_CREAT)) == -1) {
    perror("FAIL");
    return 1;
  }
  if ((seconds = (int *)shmat(shm_id_seconds, NULL, 0)) == (void *)-1) {
    perror("Failed to attach shared memory segment");
    if (shmctl(shm_id_seconds, IPC_RMID, NULL) == -1)
      perror("Failed to  remove memory segment");
      return 1;
  }

  //get shared memory for nano_seconds;
  if ((shm_id_nano_seconds = shmget(key_nano_seconds, sizeof(int), PERM | IPC_CREAT)) == -1) {
    perror("FAIL");
    return 1;
  }
  if ((nano_seconds = (int*)shmat(shm_id_nano_seconds, NULL, 0)) == (void *)-1) {
    perror("Failed to attach shared memory segment");
    if (shmctl(shm_id_nano_seconds, IPC_RMID, NULL) == -1)
      perror("Failed to  remove memory segment");
      return 1;
  }

//fill resources

    fill_resources(all_resources);


 /* for(i = percent_shareable_resource; i < NUM_RESOURCES; i++) {


    (&all_resources[i])->total_resources = rand()% 10 + 1;

  } */

  memset(pids, 0, sizeof(pids));

//open queues

  child_queueid = initqueue(child_queue_key, child_queueid);
  parent_queueid = initqueue(parent_queue_key, parent_queueid);
  kill_queueid = initqueue(kill_queue_key, kill_queueid);
  time_queueid = initqueue(time_queue_key, time_queueid);
  deadlock_queueid = initqueue(deadlock_queue_key, deadlock_queueid);
  deadlockrec_queueid = initqueue(deadlockrec_queue_key, deadlockrec_queueid);


 /* for(i = 0; i < NUM_RESOURCES; i++) {

    printf("%d, %s\n", (&all_resources[i])->all_available, (&all_resources[i])->resource_name);
  }*/

//send first message for child processes to be able to get in critical section

  timemsg.mtype = 1;
  if(msgsnd(time_queueid, &timemsg, 1024,0) == -1) {
    perror("Failed write");
    return 1;
  }

//set up timers for actions of oss

  when_to_spawn = SPAWN_PROC
  int when_to_deadlock = (*seconds * MULTIPLY_FOR_SECOND) + *nano_seconds + 500000;
  sprintf(send_str, "%d", total_num_of_proc);

    alarm(2);
  //printf("%d\n", number_proc);

//main loop

  while (1) {

//spawn new process at timer

    if (((*seconds * MULTIPLY_FOR_SECOND) + *nano_seconds) >= when_to_spawn && number_proc < total_num_of_proc) {

      waitpid(-1, 0, WNOHANG);
      number_proc++;
      child_pid = fork();

      if (child_pid < 0) {

        printf("Could not fork!");
        return 1;
      }

      else if(child_pid > 0)  {

        //when_to_spawn = SPAWN_PROC
        //printf("new proc\n");
        for (x = 0; x < total_num_of_proc; x++) {

         if(pids[x] == 0) {

            pids[x] = child_pid;
            break;
          }
        }
      }

      else  {

       //  printf("in child\n");
         //exit(0);
        execl("./slave", "slave", send_str, 0);
      }
    }

//when message received for requesting a resource

    if ((msgrcv(parent_queueid, &sentmsg, 1024, 0, IPC_NOWAIT)) >=0) {
  /*for(i = 0; i < NUM_RESOURCES; i++) {

    printf("%d, %s\n", (&all_resources[i])->total_resources, (&all_resources[i])->resource_name);
  }*/

//     for (x = 0; x < total_num_of_proc; x++) {

//        if(pids[x] == sentmsg.mtype)
//          break;
//      }

      x = get_pid_number(pids, total_num_of_proc, sentmsg);

     // printf("request");

//      for (y = 0; y < NUM_RESOURCES; y++) {

  //      if(!strcmp(sentmsg.mtext, ((&all_resources[y])->resource_name))) {

      //    (&all_resources[y])->request[x] = (&all_resources[y])->request[x] + 1;
    //      break;
    //    }
     // }

      if(x != -1) {
        y = get_res_num(sentmsg, all_resources);
        (&all_resources[y])->request[x] = (&all_resources[y])->request[x] + 1;
        //printf("HI");

        if(num_lines < TOTAL_LINES && verbose == 1) {

          fprintf(logfile, "Master has detected Process %d requesting %s at time %d:%d\n", pids[x], (&all_resources[y])->resource_name, *seconds, *nano_seconds);
          num_lines++;
   //printf("HI");
        }

        if ((&all_resources[y])->total_resources != 0) {

          (&all_resources[y])->allocation[x] = (&all_resources[y])->allocation[x] + 1;
          (&all_resources[y])->request[x] = (&all_resources[y])->request[x] - 1;
          (&all_resources[y])->total_resources = (&all_resources[y])->total_resources - 1;
          granted++;

          if(num_lines < TOTAL_LINES && verbose == 1) {

            fprintf(logfile, "Master granting %d request %s at time %d:%d\n", pids[x], (&all_resources[y])->resource_name, *seconds, *nano_seconds);
            num_lines++;
          }
        }
      }
    }

//when message received for releasing a resource

    if ((msgrcv(child_queueid, &sentmsg, 1024, 0, IPC_NOWAIT)) >=0) {


//      for (x = 0; x < total_num_of_proc; x++) {
//
    //    if(pids[x] == sentmsg.mtype)
  //        break;
     // }

      x = get_pid_number(pids, total_num_of_proc, sentmsg);

    // printf("%d\n", x);

//      for (y = 0; y < NUM_RESOURCES; y++) {

  //      if(!strcmp(sentmsg.mtext, (&all_resources[y])->resource_name)) {

    //      (&all_resources[y])->release[x] = (&all_resources[y])->release[x] + 1;
      //    break;
      //  }
    //  }

      if(x != -1) {
        y = get_res_num(sentmsg, all_resources);
       // (&all_resources[y])->release[x] = (&all_resources[y])->release[x] + 1;
        (&all_resources[y])->allocation[x] = (&all_resources[y])->allocation[x] - 1;
        (&all_resources[y])->release[x] = (&all_resources[y])->release[x] - 1;
        (&all_resources[y])->total_resources = (&all_resources[y])->total_resources + 1;

        if(num_lines < TOTAL_LINES && verbose == 1) {

          fprintf(logfile, "Master has acknowledged Process %d releasing %s at time %d:%d\n", pids[x],(&all_resources[y])->resource_name, *seconds, *nano_seconds);
          num_lines++;
        }
        for (x=0; x < total_num_of_proc; x++) {

          if((&all_resources[y])->request[x] > 0) {

            (&all_resources[y])->request[x] = (&all_resources[y])->request[x] - 1;
            (&all_resources[y])->total_resources = (&all_resources[y])->total_resources - 1;
            granted++;

            if(num_lines < TOTAL_LINES && verbose == 1) {

              fprintf(logfile, "Master granting %d request %s at time %d:%d\n", pids[x], (&all_resources[y])->resource_name, *seconds, *nano_seconds);
              num_lines++;

            }
            break;
          }
        }
      }
    }

//when process terminates

    if ((msgrcv(kill_queueid, &sentmsg, 1024, 0, IPC_NOWAIT)) >=0) {

      x = get_pid_number(pids, total_num_of_proc, sentmsg);

     /* int r =0;
      while (r < 500)
        r++;*/
      if(x != -1) {

        reclaim_resources(all_resources, x);
        giveout_resources(all_resources, total_num_of_proc, &num_lines, verbose);
        pids[x] = 0;
        success_term++;
        number_proc--;
      }
    }

//when to run deadlock

    if(number_proc > 0 && (*seconds * MULTIPLY_FOR_SECOND) + *nano_seconds >= when_to_deadlock ) {

     //printf("%d\n", number_proc);
      if (num_lines < TOTAL_LINES) {

        fprintf(logfile, "Master running deadlock detection at time %d:%d\n", *seconds, *nano_seconds);
        num_lines++;
      }
      //printf("%d\n", number_proc);
      detection_ran++;
      while(deadlock(all_resources, &number_proc, pids, &num_lines, total_num_of_proc, verbose)) {

        detection_ran++;
        if (num_lines < TOTAL_LINES) {

          fprintf(logfile, "\t Master running deadlock after killing process\n");
          num_lines++;
        }
      }
      when_to_deadlock = (*seconds * MULTIPLY_FOR_SECOND) + *nano_seconds + 100000;

      if (num_lines < TOTAL_LINES) {

        fprintf(logfile, "\tDeadlock detection complete\n");
        num_lines++;
      }
    }


    *nano_seconds = *nano_seconds + 5000;
    //printf("%d\n", *nano_seconds);

    waitpid(-1, 0, WNOHANG);

    if (*nano_seconds >= MULTIPLY_FOR_SECOND) {

      *nano_seconds = *nano_seconds - MULTIPLY_FOR_SECOND;
      *seconds = *seconds + 1;
    }

    if(*seconds >= 2) {

      printf("2 seconds reached\n");
      break;
    }

  }

  for(i = 0; i < NUM_RESOURCES; i++) {

    printf("%d, %s\n", (&all_resources[i])->total_resources, (&all_resources[i])->resource_name);
  }

//kill processes

  num_proc_for_alarm = number_proc;
  kill_childs();

//remove shared memory

  if (detachandremove(shm_id_seconds, seconds) == -1) {
    perror("Failed to destroy shared memory segment");
    return 1;
  }

  if (detachandremove(shm_id_nano_seconds, nano_seconds) == -1) {
    perror("Failed to destroy shared memory segment");
    return 1;
  }

  if (detachandremove(shm_id_resources, all_resources) == -1) {
    perror("Failed to destroy shared memory segment");
    return 1;
  }

  if (detachandremove(shm_id_pids, pids) == -1) {
    perror("Failed to destroy shared memory segment");
    return 1;
  }

//remove queues

  if(removequeue(parent_queueid) < 0)
    perror("remove queue");
  if(removequeue(child_queueid) < 0)
    perror("remove queue");
  if(removequeue(kill_queueid) < 0)
    perror("remove queue");
  if(removequeue(time_queueid) < 0)
    perror("remove queue");
  if(removequeue(deadlock_queueid) < 0)
    perror("remove queue");
  if(removequeue(deadlockrec_queueid) < 0)
    perror("remove queue");

//print stats

  statistics();

//close file


  fclose(logfile);

  return 0;
}
