//msgqueue
//This is the msgqueue interface
//Justin Henn
//Assignement 3
//3/05/17

#include <errno.h>
#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>
#include <sys/msg.h>
#include <sys/stat.h>
#include "msgqueue.h"
#define PERM (S_IRUSR | S_IWUSR)

//structure for the message

typedef struct {
   long mtype;
   char mtext[1];
} mymsg_t;
//int static queueid;

int initqueue(int key, int queueid) {                    /* initialize the message queue */

   queueid = msgget(key, PERM | IPC_CREAT);
   if (queueid == -1) {
      perror("msgget");
      exit(1);
   }
   return queueid;
}

//fuction used to connect slave programs to queues created in master

int initchildprocqueue(int key, int queueid) {                   

   queueid = msgget(key, PERM);
   if (queueid == -1) {
      perror("msgget");
      exit(1);
   }
   return queueid;
}

int msgprintf(int queueid, char *fmt, ...) {               /* output a formatted message */
   va_list ap;
   char ch;
   int error = 0;
   int len;
   mymsg_t *mymsg;
 
   va_start(ap, fmt);                       /* set up the format for output */
   len = vsnprintf(&ch, 1, fmt, ap);              /* how long would it be ? */
   if ((mymsg = (mymsg_t *)malloc(sizeof(mymsg_t) + len)) == NULL)
      return -1;
   vsprintf(mymsg->mtext, fmt, ap);                 /* copy into the buffer */
   //printf("sucks\n");
   mymsg->mtype = 1;                            /* message type is always 1 */
   if (msgsnd(queueid, mymsg, len + 1, 0) == -1) 
      error = errno;
   free(mymsg);
   if (error) {
      errno = error;
      return -1;
   }
   return 0;
}

int msgread(int queueid, void *buf, int len) {   /* read into buffer of specified length */
   return msgrcv(queueid, buf, len, 0, 0);
}

/*int msgwrite(void *buf, int len) {      output buffer of specified length  
   int error = 0;
   mymsg_t *mymsg;
   
   if ((mymsg = (mymsg_t *)malloc(sizeof(mymsg_t) + len - 1)) == NULL)
      return -1;
   memcpy(mymsg->mtext, buf, len);
   mymsg->mtype = 1;                             message type is always 1 
   if (msgsnd(queueid, mymsg, len, 0) == -1)
      error = errno;
   free(mymsg);
   if (error) {
      errno = error;
      return -1;
   }
   return 0;
}*/

//remove queues
int removequeue(int queueid) { 
   return msgctl(queueid, IPC_RMID, NULL);
}
